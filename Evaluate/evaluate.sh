#!/bin/bash

# Note: Only 'R-GCN', 'HAN', 'MAGNN', and 'HGT' support attributed='True' or supervised='True'
# Note: Only 'DBLP' and 'PubMed' support attributed='True'

dataset='PubMed' # choose from 'DBLP', 'Yelp', 'Freebase', and 'PubMed'
model=$1 # choose from 'metapath2vec-ESim', 'PTE', 'HIN2Vec', 'AspEm', 'HEER', 'R-GCN', 'HAN', 'MAGNN', 'HGT', 'TransE', 'DistMult', 'ComplEx', and 'ConvE'
task='nc' # choose 'nc' for node classification, 'lp' for link prediction, or 'both' for both tasks
attributed=$2 # choose 'True' or 'False'
supervised=$3 # choose 'True' or 'False'
index_classifier=$4 # choose 0 1 2 3 4 5 6
index_com_nc=$5 #choose 0 or 1 for community detection or node classification

python evaluate.py -dataset ${dataset} -model ${model} -task ${task} -attributed ${attributed} -supervised ${supervised} -index_classifier ${index_classifier} -index_com_nc ${index_com_nc}
