import warnings
import numpy as np
from collections import defaultdict
from sklearn.svm import LinearSVC
from sklearn.metrics import f1_score, accuracy_score, auc, ndcg_score, precision_score, recall_score, roc_auc_score
from sklearn.model_selection import StratifiedKFold
from sklearn.exceptions import UndefinedMetricWarning, ConvergenceWarning
from catboost import CatBoostClassifier
import xgboost as xgb
from skranger.ensemble import RangerForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans, AffinityPropagation, MeanShift, SpectralClustering, AgglomerativeClustering, DBSCAN, OPTICS, Birch
from sklearn.metrics.cluster import adjusted_rand_score, normalized_mutual_info_score

seed = 1
max_iter = 300
np.random.seed(seed)
warnings.filterwarnings("ignore", category=ConvergenceWarning)
warnings.filterwarnings("ignore", category=UndefinedMetricWarning)


def nc_evaluate(dataset, supervised, label_file_path, label_test_path, emb_dict,index_classifier, index_com_nc):    
    if(index_com_nc=="1"):
        if supervised=='True':
            if dataset=='Yelp':
                return semisupervised_single_class_multi_label(label_file_path, label_test_path, emb_dict)
            elif dataset=='DBLP' or dataset=='Freebase' or dataset=='PubMed':
                return semisupervised_single_class_single_label(label_file_path, label_test_path, emb_dict,index_classifier)
        elif supervised=='False':
            if dataset=='Yelp':
                return unsupervised_single_class_multi_label(label_file_path, label_test_path, emb_dict)
            elif dataset=='DBLP' or dataset=='Freebase' or dataset=='PubMed':
                return unsupervised_single_class_single_label(label_file_path, label_test_path, emb_dict,index_classifier)

    elif(index_com_nc=="0"):
      if(supervised=='False'):
        if(dataset=='PubMed'):
          return community_detection_module(label_file_path,label_test_path,emb_dict,index_classifier)
      elif(supervised=='True'):
        if(dataset=='PubMed'):
          return community_detection_module_supervised(label_file_path,label_test_path,emb_dict,index_classifier)


def semisupervised_single_class_single_label(label_file_path, label_test_path, emb_dict, index_classifier):
    macro, micro = [], []
    accuracy, precision_value_micro, recall_value_micro, precision_value_macro, recall_value_macro, nmi_score = [], [], [], [], [], []
    
    train_labels, train_embeddings = [], []
    with open(label_file_path,'r') as label_file:
        for line in label_file:
            index, _, _, label = line[:-1].split('\t')
            train_labels.append(label)
            train_embeddings.append(emb_dict[index])    
    train_labels, train_embeddings = np.array(train_labels).astype(int), np.array(train_embeddings)  
    
    test_labels, test_embeddings = [], []
    with open(label_test_path,'r') as label_file:
        for line in label_file:
            index, _, _, label = line[:-1].split('\t')
            test_labels.append(label)
            test_embeddings.append(emb_dict[index])    
    test_labels, test_embeddings = np.array(test_labels).astype(int), np.array(test_embeddings)  
        
    if(index_classifier=="0"):
      clf = LinearSVC(random_state=seed, max_iter=max_iter)
      clf.fit(train_embeddings, train_labels)
      preds = clf.predict(test_embeddings)
    
    if(index_classifier=="1"):
      learning_rate,depth=0.1,10
      clf = CatBoostClassifier(iterations=max_iter,learning_rate=learning_rate,depth=depth)
      clf.fit(train_embeddings, train_labels)
      preds = clf.predict(test_embeddings)

    if(index_classifier=="2"):
      clf = xgb.XGBClassifier(n_jobs=1).fit(train_embeddings, train_labels)
      preds = clf.predict(test_embeddings)

    if(index_classifier=="3"):
      clf = RangerForestClassifier()
      clf.fit(train_embeddings, train_labels)
      preds = clf.predict(test_embeddings)
      
    if(index_classifier=="4"):
      clf = MLPClassifier(max_iter=max_iter)
      clf.fit(train_embeddings, train_labels)
      preds = clf.predict(test_embeddings)
      
    if(index_classifier=="5"):
      clf = RandomForestClassifier()
      clf.fit(train_embeddings, train_labels)
      preds = clf.predict(test_embeddings)
    
    if(index_classifier=="6"):
      clf = LogisticRegression(random_state=seed, max_iter=max_iter)
      clf.fit(train_embeddings, train_labels)
      preds = clf.predict(test_embeddings)
	
    macro.append(f1_score(test_labels, preds, average='macro'))
    micro.append(f1_score(test_labels, preds, average='micro'))
    accuracy.append(accuracy_score(test_labels,preds))
    nmi_score.append(normalized_mutual_info_score(test_labels,preds.flatten()))
    precision_value_macro.append(precision_score(test_labels,preds,average='macro'))
    precision_value_micro.append(precision_score(test_labels,preds,average='micro'))
    recall_value_macro.append(recall_score(test_labels,preds,average='macro'))
    recall_value_micro.append(recall_score(test_labels,preds,average='micro'))
    print("F1_macro",macro)
    print("F1_micro",micro)
    print("Precision_macro",precision_value_macro)
    print("Precision_micro",precision_value_micro)
    print("Recall_macro",recall_value_macro)
    print("Recall_micro",recall_value_micro)
    print("NMI",nmi_score)

    return macro[0], micro[0], accuracy[0]
        
    
def unsupervised_single_class_single_label(label_file_path, label_test_path, emb_dict, index_classifier):
    
    labels, embeddings = [], []    
    for file_path in [label_file_path, label_test_path]:
        with open(file_path,'r') as label_file:
            for line in label_file:
                index, _, _, label = line[:-1].split('\t')
                labels.append(label)
                embeddings.append(emb_dict[index])    
    labels, embeddings = np.array(labels).astype(int), np.array(embeddings)  
    
    macro, micro = [], []
    accuracy, precision_value_micro, recall_value_micro, precision_value_macro, recall_value_macro, nmi_score = [], [], [], [], [], []
    skf = StratifiedKFold(n_splits=5,shuffle=True,random_state=seed)
    for train_idx, test_idx in skf.split(embeddings, labels):
      test_labels=labels[test_idx]
      if(index_classifier=="0"):
        clf = LinearSVC(random_state=seed, max_iter=max_iter)
        clf.fit(embeddings[train_idx], labels[train_idx])
        preds = clf.predict(embeddings[test_idx])

      if(index_classifier=="1"):
        learning_rate,depth=0.1,10
        clf = CatBoostClassifier(iterations=max_iter,learning_rate=learning_rate,depth=depth)
        clf.fit(embeddings[train_idx], labels[train_idx])
        preds = clf.predict(embeddings[test_idx])

      if(index_classifier=="2"):
        clf = xgb.XGBClassifier(n_jobs=1).fit(embeddings[train_idx], labels[train_idx])
        preds = clf.predict(embeddings[test_idx])

      if(index_classifier=="3"):
        clf = RangerForestClassifier()
        clf.fit(embeddings[train_idx], labels[train_idx])
        preds = clf.predict(embeddings[test_idx])
      
      if(index_classifier=="4"):
        clf = MLPClassifier(max_iter=max_iter)
        clf.fit(embeddings[train_idx], labels[train_idx])
        preds = clf.predict(embeddings[test_idx])
      
      if(index_classifier=="5"):
        clf = RandomForestClassifier()
        clf.fit(embeddings[train_idx], labels[train_idx])
        preds = clf.predict(embeddings[test_idx])
      
      if(index_classifier=="6"):
        clf = LogisticRegression(random_state=seed, max_iter=max_iter)
        clf.fit(embeddings[train_idx], labels[train_idx])
        preds = clf.predict(embeddings[test_idx])
        
      macro.append(f1_score(labels[test_idx], preds, average='macro'))
      micro.append(f1_score(labels[test_idx], preds, average='micro'))
      accuracy.append(accuracy_score(test_labels,preds))
      #auc_value.append(auc(test_labels,preds))
      #ngdc_value.append(ndcg_score(test_labels,preds))
      nmi_score.append(normalized_mutual_info_score(labels[test_idx],preds.flatten()))
      precision_value_macro.append(precision_score(labels[test_idx],preds,average='macro'))
      precision_value_micro.append(precision_score(labels[test_idx],preds,average='micro'))
      recall_value_macro.append(recall_score(labels[test_idx],preds,average='macro'))
      recall_value_micro.append(recall_score(labels[test_idx],preds,average='micro'))
      #rocauc_value.append(roc_auc_score(np.argmax(labels[test_idx],axis=0),np.argmax(preds,axis=0),multi_class='ovo'))
    print("F1_macro",macro)
    print("F1_micro",micro)
    print("Precision_macro",precision_value_macro)
    print("Precision_micro",precision_value_micro)
    print("Recall_macro",recall_value_macro)
    print("Recall_micro",recall_value_micro)
    print("NMI",nmi_score)
			#np.mean(auc_value), np.mean(ngdc_value), np.mean(rocauc_value)
    return np.mean(macro), np.mean(micro), np.mean(accuracy)


def semisupervised_single_class_multi_label(label_file_path, label_test_path, emb_dict):

    nodes_count, binary_labels, label_dict, label_count, train_nodes, test_nodes = len(emb_dict), [], {}, 0, set(), set()
    for file_path in [label_file_path, label_test_path]:
        with open(file_path,'r') as label_file:
            for line in label_file:
                index, _, nclass, label = line[:-1].split('\t')   
                for each in label.split(','):                
                    if (nclass, each) not in label_dict:
                        label_dict[(nclass, each)] = label_count
                        label_count += 1
                        binary_labels.append(np.zeros(nodes_count).astype(np.bool_))
                    binary_labels[label_dict[(nclass, each)]][int(index)] = True
                    if file_path==label_file_path: train_nodes.add(int(index))
                    else: test_nodes.add(int(index))
    train_nodes, test_nodes = np.sort(list(train_nodes)), np.sort(list(test_nodes))
    train_labels, test_labels = np.array(binary_labels)[:,train_nodes], np.array(binary_labels)[:,test_nodes]
    
    train_embs, test_embs = [], []
    for index in train_nodes:
        train_embs.append(emb_dict[str(index)])
    for index in test_nodes:
        test_embs.append(emb_dict[str(index)])
    train_embs, test_embs = np.array(train_embs), np.array(test_embs)
    
    weights, total_scores = [], []
    for ntype, (train_label, test_label) in enumerate(zip(train_labels, test_labels)):           
        
        clf = LinearSVC(random_state=seed, max_iter=max_iter)
        clf.fit(train_embs, train_label)
        preds = clf.predict(test_embs)
        scores = append(f1_score(test_label, preds, average='binary'))

        weights.append(sum(test_label))
        total_scores.append(scores)
        
    macro = sum(total_scores)/len(total_scores)
    micro = sum([score*weight for score, weight in zip(total_scores, weights)])/sum(weights)
    
    return macro, micro


def unsupervised_single_class_multi_label(label_file_path, label_test_path, emb_dict):

    nodes_count, binary_labels, label_dict, label_count, labeled_nodes = len(emb_dict), [], {}, 0, set()
    for file_path in [label_file_path, label_test_path]:
        with open(file_path,'r') as label_file:
            for line in label_file:
                index, _, nclass, label = line[:-1].split('\t')   
                for each in label.split(','):                
                    if (nclass, each) not in label_dict:
                        label_dict[(nclass, each)] = label_count
                        label_count += 1
                        binary_labels.append(np.zeros(nodes_count).astype(np.bool_))
                    binary_labels[label_dict[(nclass, each)]][int(index)] = True
                    labeled_nodes.add(int(index))
    labeled_nodes = np.sort(list(labeled_nodes))
    binary_labels = np.array(binary_labels)[:,labeled_nodes]
    
    embs = []
    for index in labeled_nodes:
        embs.append(emb_dict[str(index)])
    embs = np.array(embs)
    
    weights, total_scores = [], []
    for ntype, binary_label in enumerate(binary_labels):
        
        scores = []
        skf = StratifiedKFold(n_splits=5,shuffle=True,random_state=seed)
        for train_idx, test_idx in skf.split(embs, binary_label):            
        
            clf = LinearSVC(random_state=seed, max_iter=max_iter)
            clf.fit(embs[train_idx], binary_label[train_idx])
            preds = clf.predict(embs[test_idx])
            scores.append(f1_score(binary_label[test_idx], preds, average='binary'))

        weights.append(sum(binary_label))
        total_scores.append(sum(scores)/5)
        
    macro = sum(total_scores)/len(total_scores)
    micro = sum([score*weight for score, weight in zip(total_scores, weights)])/sum(weights)
    
    return macro, micro

def community_detection_module(label_file_path, label_test_path, emb_dict,index_classifier):
    macro, micro = [], []
    accuracy, precision_value_micro, recall_value_micro, precision_value_macro, recall_value_macro, nmi_score = [], [], [], [], [], []
    
    labels, embeddings = [], []    
    for file_path in [label_file_path, label_test_path]:
        with open(file_path,'r') as label_file:
            for line in label_file:
                index, _, _, label = line[:-1].split('\t')
                labels.append(label)
                embeddings.append(emb_dict[index])    
    labels, embeddings = np.array(labels).astype(int), np.array(embeddings)  
    
    if(index_classifier=="0"):
      clustering = KMeans(n_clusters=2, max_iter=max_iter, random_state=0).fit(embeddings)
    elif(index_classifier=="1"):
      clustering = AffinityPropagation(max_iter=max_iter).fit(embeddings)
    elif(index_classifier=="2"):
      clustering = MeanShift(max_iter=max_iter, bandwidth=2).fit(embeddings)
    elif(index_classifier=="3"):
      clustering = SpectralClustering(n_clusters=2, assign_labels="discretize", random_state=0).fit(embeddings)
    elif(index_classifier=="4"):
      clustering = AgglomerativeClustering(n_clusters=2).fit(embeddings)
    elif(index_classifier=="5"):
      #algorithm{'auto', 'ball_tree', 'kd_tree', 'brute'}
      clustering = DBSCAN(eps=3, algorithm='auto').fit(embeddings)
    elif(index_classifier=="6"):
      #algorithm{'auto', 'ball_tree', 'kd_tree', 'brute'}
      clustering = OPTICS(min_samples=2).fit(embeddings)
    elif(index_classifier=="7"):
      clustering=Birch(n_clusters=None).fit(embeddings)

    preds=clustering.labels_

    macro.append(f1_score(labels, preds, average='macro'))
    micro.append(f1_score(labels, preds, average='micro'))
    accuracy.append(accuracy_score(labels,preds))
    nmi_score.append(normalized_mutual_info_score(labels,preds.flatten()))
    precision_value_macro.append(precision_score(labels,preds,average='macro'))
    precision_value_micro.append(precision_score(labels,preds,average='micro'))
    recall_value_macro.append(recall_score(labels,preds,average='macro'))
    recall_value_micro.append(recall_score(labels,preds,average='micro'))
    print("F1_macro",macro)
    print("F1_micro",micro)
    print("Precision_macro",precision_value_macro)
    print("Precision_micro",precision_value_micro)
    print("Recall_macro",recall_value_macro)
    print("Recall_micro",recall_value_micro)
    print("NMI",nmi_score)

    return nmi_score[0]	

def community_detection_module_supervised(label_file_path, label_test_path, emb_dict,index_classifier):
    macro, micro = [], []
    accuracy, precision_value_micro, recall_value_micro, precision_value_macro, recall_value_macro, nmi_score = [], [], [], [], [], []
    
    train_labels, train_embeddings = [], []
    with open(label_file_path,'r') as label_file:
        for line in label_file:
            index, _, _, label = line[:-1].split('\t')
            train_labels.append(label)
            train_embeddings.append(emb_dict[index])    
    train_labels, train_embeddings = np.array(train_labels).astype(int), np.array(train_embeddings)  
    
    test_labels, test_embeddings = [], []
    with open(label_test_path,'r') as label_file:
        for line in label_file:
            index, _, _, label = line[:-1].split('\t')
            test_labels.append(label)
            test_embeddings.append(emb_dict[index])    
    test_labels, test_embeddings = np.array(test_labels).astype(int), np.array(test_embeddings) 
      
    if(index_classifier=="0"):
      clustering = KMeans(n_clusters=2, max_iter=max_iter, random_state=0).fit(test_embeddings)
    elif(index_classifier=="1"):
      clustering = AffinityPropagation(max_iter=max_iter).fit(test_embeddings)
    elif(index_classifier=="2"):
      clustering = MeanShift(max_iter=max_iter, bandwidth=2).fit(test_embeddings)
    elif(index_classifier=="3"):
      clustering = SpectralClustering(n_clusters=2, assign_labels="discretize", random_state=0).fit(test_embeddings)
    elif(index_classifier=="4"):
      clustering = AgglomerativeClustering(n_clusters=2).fit(test_embeddings)
    elif(index_classifier=="5"):
      #algorithm{'auto', 'ball_tree', 'kd_tree', 'brute'}
      clustering = DBSCAN(eps=3, algorithm='auto').fit(test_embeddings)
    elif(index_classifier=="6"):
      #algorithm{'auto', 'ball_tree', 'kd_tree', 'brute'}
      clustering = OPTICS(min_samples=2).fit(test_embeddings)
    elif(index_classifier=="7"):
      clustering=Birch(n_clusters=None).fit(test_embeddings)

    preds=clustering.labels_
    labels=test_labels

    macro.append(f1_score(labels, preds, average='macro'))
    micro.append(f1_score(labels, preds, average='micro'))
    accuracy.append(accuracy_score(labels,preds))
    nmi_score.append(normalized_mutual_info_score(labels,preds.flatten()))
    precision_value_macro.append(precision_score(labels,preds,average='macro'))
    precision_value_micro.append(precision_score(labels,preds,average='micro'))
    recall_value_macro.append(recall_score(labels,preds,average='macro'))
    recall_value_micro.append(recall_score(labels,preds,average='micro'))
    print("F1_macro",macro)
    print("F1_micro",micro)
    print("Precision_macro",precision_value_macro)
    print("Precision_micro",precision_value_micro)
    print("Recall_macro",recall_value_macro)
    print("Recall_micro",recall_value_micro)
    print("NMI",nmi_score)

    return nmi_score[0]
