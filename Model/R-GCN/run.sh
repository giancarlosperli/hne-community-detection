#!/bin/bash

dataset="PubMed"
folder="data/${dataset}/"
node_file="${folder}node.dat"
link_file="${folder}link.dat"
label_file="${folder}label.dat"
emb_file="${folder}emb.dat"

size=$1 # embedding dimension
negative=$2 # number of negative samples
lr=$3 # initial learning rate
dropout=$4
regularization=$5
grad_norm=1.0
edge_sampler=$6
gpu=0
num_bases=-1
num_layers=2
num_epochs=$7
graph_batch_size=20000
graph_split_size=0.5
label_batch_size=64

attributed=$8
supervised=$9

python3 src/main.py --link ${link_file} --node ${node_file} --label ${label_file} --output ${emb_file} --n-hidden ${size} --negative-sample ${negative} --lr ${lr} --dropout ${dropout} --gpu ${gpu} --n-bases ${num_bases} --n-layers ${num_layers} --n-epochs ${num_epochs} --regularization ${regularization} --grad-norm ${grad_norm} --graph-batch-size ${graph_batch_size} --graph-split-size ${graph_split_size} --label-batch-size ${label_batch_size} --edge-sampler ${edge_sampler} --attributed ${attributed} --supervised ${supervised}
