#!/bin/bash

dataset="PubMed"
folder="/content/hne-community-detection/Data/${dataset}/"
link_file="${folder}link.dat"
emb_file="${folder}emb.dat"

size=$1 # embedding dimension

python src/main.py --dimensions ${size} --input ${link_file} --output ${emb_file}
