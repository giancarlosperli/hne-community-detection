#!/bin/bash

dataset="PubMed"
folder="/content/hne-community-detection/Data/${dataset}/"
link_file="${folder}link.dat"
emb_file="${folder}emb.dat"

python setup.py install
size=$1

deepwalk --input ${link_file} --output ${emb_file} --representation-size ${size}
